import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { createBrowserHistory } from "history";

//Routes

import Login from "./Pages/Login";
import Registration from "./Pages/Registration";
import View from "./Pages/View";
import Home from "./Pages/Home";

const historyObj = createBrowserHistory();

export default class App extends Component {
  render() {
    return (
      <div>
        <Router history={historyObj}>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/login" component={Login} />
            <Route path="/registration" component={Registration} />
            <Route path="/view" component={View} />
          </Switch>
        </Router>
      </div>
    );
  }
}
