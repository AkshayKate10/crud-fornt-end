import React, { useState, useEffect } from "react";
import axios from "axios";

export default function Registration(props) {
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [age, setAge] = useState("");
  const [sex, setSex] = useState("");
  const [response, setResponse] = useState("");
  const [showError, setShowError] = useState(false);

  useEffect(() => {
    if (response.error || response.message) setShowError(true);
  }, [response]);

  const url = "http://127.0.0.1:3001/register";

  const onClickHome = () => {
    const { history } = props;
    history.push("/");
  };
  const onChangeUserName = (e) => {
    setUserName(e.target.value);
    setShowError(false);
  };
  const onChangePassword = (e) => {
    setPassword(e.target.value);
    setShowError(false);
  };
  const onChangeAge = (e) => {
    setAge(e.target.value);
    setShowError(false);
  };
  const onChangeSex = (e) => {
    setSex(e.target.value);
    setShowError(false);
  };
  const onClickRegister = () => {
    axios({
      url,
      method: "post",
      data: { userName, password, age, sex },
    }).then((response) => {
      setResponse(response.data);
    });
    console.log("clicked");
  };

  return (
    <div>
      <h1>This is Registration</h1>
      <div>
        <div>{showError && response && response.message}</div>
        <div>{showError && response && response.error}</div>
        <div>
          User Name : <input onChange={onChangeUserName} />
        </div>
        <div>
          Password : <input onChange={onChangePassword} />
        </div>
        <div>
          Age : <input onChange={onChangeAge} />
        </div>
        {/* <div>
          Sex :
          <input type="radio" id="male" name="gender" value="male" />
          <label for="male">Male</label>
          <input type="radio" id="female" name="gender" value="female" />
          <label for="female">Female</label>
        </div> */}
        <div>
          Sex :<input onChange={onChangeSex} />
        </div>
      </div>
      <div>
        <button onClick={onClickRegister}>Register</button>
      </div>

      <div>
        <button onClick={onClickHome}>Home</button>
      </div>
    </div>
  );
}
