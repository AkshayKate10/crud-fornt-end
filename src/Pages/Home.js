import React from "react";

export default function Home(props) {
  const onClickPageChange = (pageName) => {
    const { history } = props;

    history.push(`/${pageName}`);
  };
  return (
    <div>
      <h1>This is Home</h1>
      <div>
        <button
          onClick={() => {
            onClickPageChange("Login");
          }}
        >
          Login Page
        </button>
      </div>
      <div>
        <button
          onClick={() => {
            onClickPageChange("Registration");
          }}
        >
          Registration Page
        </button>
      </div>
      <div>
        <button
          onClick={() => {
            onClickPageChange("View");
          }}
        >
          View Page
        </button>
      </div>
    </div>
  );
}
