import React, { useEffect, useState } from "react";
import axios from "axios";
import { isEmpty } from "lodash";

export default function Login(props) {
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [response, setResponse] = useState("");
  const [showError, setShowError] = useState(false);

  const url = "http://127.0.0.1:3001/login";
  const onClickHome = () => {
    const { history } = props;
    history.push("/");
  };

  useEffect(() => {
    if (isEmpty(response)) {
      setShowError(true);
    } else {
      setShowError(false);
    }
  }, [response]);

  const onClickLogin = () => {
    // axios.get(url);
    axios({
      method: "post",
      url,
      data: {
        userName,
        password,
      },
    }).then((response) => {
      setResponse(response.data);

      console.log("response", response);
    });
    console.log("userName", userName);
    console.log("password", password);
  };

  const onChangeUserName = (e) => {
    setUserName(e.target.value);
  };

  const onChangePassword = (e) => {
    setPassword(e.target.value);
  };

  return (
    <div>
      <h1>This is Login</h1>
      <h2>{showError ? "Please enter correct details" : ""}</h2>
      <div>
        User Name : <input onChange={(e) => onChangeUserName(e)} />
      </div>
      <div>
        Password : <input onChange={(e) => onChangePassword(e)} />
      </div>
      <div>
        <button onClick={onClickLogin}>Login</button>
      </div>
      <div>
        <button onClick={onClickHome}>Home</button>
      </div>
      <h2>{!showError ? "Login Successful" : ""}</h2>
    </div>
  );
}
