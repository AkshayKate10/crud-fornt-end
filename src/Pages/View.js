import React, { useState, useEffect } from "react";
import axios from "axios";
import { isEmpty } from "lodash";

export default function View(props) {
  const [userName, setUserName] = useState("");
  const [response, setResponse] = useState("");
  const [showError, setShowError] = useState(false);

  useEffect(() => {
    if (isEmpty(response)) {
      setShowError(true);
    } else {
      setShowError(false);
    }
  }, [response]);

  const url = "http://127.0.0.1:3001/view";
  const onClickView = () => {
    axios({
      method: "post",
      url,
      data: {
        userName,
      },
    }).then((response) => {
      setResponse(response.data[0]);

      console.log("response", response);
    });
    console.log("userName", userName);
  };
  const onClickHome = () => {
    const { history } = props;
    history.push("/");
  };

  const onChangeUserName = (e) => {
    setUserName(e.target.value);
  };
  return (
    <div>
      <h1>This is View</h1>
      <h2>{showError ? "Please enter correct details" : ""}</h2>

      <div>
        User Name: <input onChange={onChangeUserName} />
      </div>

      <div>
        <button onClick={onClickView}>View</button>
      </div>
      <div>User Name : {response && response.UserName}</div>
      <div>Age : {response && response.Age}</div>
      <div>Gender : {response && response.Gender}</div>
      <div>
        <button onClick={onClickHome}>Home</button>
      </div>
    </div>
  );
}
